package com.epam;


/**
 * Enum class for my developing.
 *
 * @author Nazarii Yaremchuk.
 * @since 15-11-2019.
 */
public enum Transparency {
    /**
     * Value type enum transparent.
     */
    TRANSPARENT,
    /**
     *  Value type enum semitransparent.
     */
    SEMITRANSPARENT,
    /**
     *  Value type enum opaque.
     */
    OPAQUE;

    /**
     *
     * @param command We use to pass the value of a variable.
     * @return the transparency value of the stone.
     * @throws InvalidCommandException can.
     */
    public static Transparency getTransparency(final String command)
            throws InvalidCommandException {
        Transparency transparency;
        switch (command) {
            case "transparent":
                transparency = TRANSPARENT;
                break;
            case "semitransparent":
                transparency = SEMITRANSPARENT;
                break;
            case "opaque":
                transparency = OPAQUE;
                break;
            default:
                throw new InvalidCommandException("Invalid transparency");
        }
        return transparency;
    }
}
