package com.epam;

/**
 * Enum class for my developing.
 *
 * @author Nazarii Yaremchuk.
 * @since 15-11-2019.
 */
public enum Value {
    /**
     * Value type enum precious stones 1 group.
     */
    PRECIOUSSTONES1GROUP,
    /**
     * Value type enum precious stones 2 group.
     */
    PRECIOUSSTONES2GROUP,
    /**
     * Value type enum precious stones 3 group.
     */
    PRECIOUSSTONES3GROUP,
    /**
     * Value type enum precious stones 4 group.
     */
    PRECIOUSSTONES4GROUP,
    /**
     * Value type enum semi precious stones 1 group.
     */
    SEMIPRECIOUSSTONES1GROUP,
    /**
     * Value type enum semi precious stones 2 group.
     */
    SEMIPRECIOUSSTONES2GROUP,
    /**
     * Value type enum manufactured stones.
     */
    MANUFACTOREDSTONES;

    /**
     *
     * @param command We use to pass the value of a variable.
     * @return type of stone.
     * @throws InvalidCommandException can.
     */
    public static Value getValue(final String command)
            throws InvalidCommandException {
        Value value;
        switch (command) {
            case "precious1":
                value = PRECIOUSSTONES1GROUP;
                break;
            case "precious2":
                value = PRECIOUSSTONES2GROUP;
                break;
            case "precious3":
                value = PRECIOUSSTONES3GROUP;
                break;
            case "precious4":
                value = PRECIOUSSTONES4GROUP;
                break;
            case "semiprecious1":
                value = SEMIPRECIOUSSTONES1GROUP;
                break;
            case "semiprecious2":
                value = SEMIPRECIOUSSTONES2GROUP;
                break;
            case "manufactured":
                value = MANUFACTOREDSTONES;
                break;
            default:
                throw new InvalidCommandException("Invalid Value");
        }
        return value;
    }
}
