package com.epam;

import com.epam.exceptions.IrrcorectStoneDataException;

import java.io.*;
import java.util.LinkedList;

/**
 * Package for my developing.
 *
 * @author Nazarii Yaremchuk.
 * @since 15-11-2019.
 */
public class Stone implements Serializable {
    /**
     * a variable that stores value stone name.
     */
    public String stoneName;
    /**
     * a variable that stores amount of stone.
     */
    public int amount;
    /**
     * a variable that stores weight of stone.
     */
    public double weight;
    /**
     * a variable that stores price of stone.
     */
    public double price;
    /**
     * a variable that stores value.
     */
    public Value value;
    /**
     * a variable that stores transparency of stone.
     */
    public Transparency transparency;
    /**
     * a variable that stores id of stone.
     */
    public Integer id;
    /**
     * a list that stores stones of stone.
     */
    public static LinkedList<Stone> stones = new LinkedList<Stone>();

    /**
     * @param sn stone name stone.
     * @param a  amount stone.
     * @param w  weight stone.
     * @param p  price stone.
     * @param v  value stone.
     * @param t  transparency stone.
     */
    public Stone(final String sn, final int a, final double w,
                 final double p, final Value v, final Transparency t) {
        if (w <= 0) {
            throw new IrrcorectStoneDataException("weight");
        }
        if (a <= 0) {
            throw new IrrcorectStoneDataException("amount");
        }
        stoneName = sn;
        amount = a;
        weight = w;
        price = p;
        value = v;
        transparency = t;
    }

    /**
     * @throws IOException            can.
     * @throws ClassNotFoundException can.
     */
    public static void loadFromFile() throws IOException,
            ClassNotFoundException {
        ObjectInputStream in;
        try {
            in = new ObjectInputStream(
                    new FileInputStream("stones.dat"));
        } catch (FileNotFoundException e) {
            return;
        }
        while (true) {
            try {
                Stone temp = (Stone) in.readObject();
                stones.add(temp);
            } catch (EOFException e) {
                break;
            }
        }
        in.close();
    }

    /**
     * @param obj a given stone.
     * @return correct answer.
     */
    public boolean equals(Stone obj) {
        if (this.stoneName.equals(obj.stoneName)) {
            if (this.transparency == obj.transparency) {
                if (this.value == obj.value) {
                    if (this.price == obj.price) {
                        if (this.weight == obj.weight) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @return name of stone.
     */
    public String toString() {
        return String.format("%s id:%d amount:%d"
                        + " weight:%f"
                        + " price:%f value:%s transparency:%s",
                stoneName, id, amount, weight, price, value.toString(),
                transparency.toString());
    }
}
