package com.epam;


import java.io.*;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class Commands implements a menu that the user can use in our program.
 *
 * @author Nazarii Yaremchuk.
 * @since 15-11-2019.
 */
class Commands implements Serializable {
    /**
     * The variable responsible for the output data stream.
     */
    private static ObjectOutputStream out;

    /**
     * Method that shows all commands.
     */
    static void viewHelp() {
        System.out.println("help: view help");
        System.out.println("add <stone> weight=<weight>"
                + " price=<price> value=<value>"
                + " transparency=<transparency>:add new stone");
        System.out.println("Values:precious1,precious2,"
                + "precious3,precious4,"
                + "semiprecious1,semiprecious2,manufactured");
        System.out.println("Transparency:transparent,"
                + "semitransparent,opaque");
        System.out.println("create <amount> :create"
                + " necklace of <amount> stones");
        System.out.println("weight:get total weight of necklace");
        System.out.println("price:get total price of necklace");
        System.out.println("sort:sort necklace by value");
        System.out.println("find<transparency> to "
                + "<transparency>:find stones from"
                + " <transparency> to <transparency> range");
        System.out.println("clear:delete all stones");
        System.out.println("delete <stone>");
        System.out.println("display necklace");
        System.out.println("display stones");
        System.out.println("exit:exit");
    }

    /**
     * @param command We use to pass the value of a variable.
     * @throws IOException             use to fix program.
     * @throws InvalidCommandException use to fix program.
     */
    static void add(String command)
            throws IOException, InvalidCommandException {
        Pattern p = Pattern.compile("^.*amount=\\d+\\"
                + "sweight=\\d+\\sprice=\\d+\\svalue=.*transparency=.*$");
        Matcher m = p.matcher(command);
        if (!m.matches()) {
            throw new InvalidCommandException("Invalid command");
        }
        Stone newStone = formStoneObject(command);
        for (Stone temp : Stone.stones) {
            if (temp.equals(newStone)) {
                changeObject(temp);
            }
        }
        Stone.stones.add(newStone);
        out = new ObjectOutputStream(
                new FileOutputStream("stones.dat", true));
        out.writeObject(newStone);
        out.close();
    }

    /**
     * @param command We use to pass the value of a variable.
     * @throws InvalidCommandException We use to pass the value of a variable.
     */
    static void create(String command) throws InvalidCommandException {
        Pattern p = Pattern.compile("^.*\\d+$");
        Matcher m = p.matcher(command);
        if (!m.matches()) {
            throw new InvalidCommandException("Invalid command");
        }
        if (Stone.stones.isEmpty()) {
            throw new InvalidCommandException("No stones added");
        }

        if (!Necklace.necklace.isEmpty()) {
            Necklace.necklace.clear();
        }
        if (command.length() == 6) {
            throw new InvalidCommandException("Invalid amount of stones in necklace");
        }
        Necklace newNecklace = new Necklace(Integer.parseInt(command.substring(command.indexOf(' '
        ) + 1)));

        for (int i = 0; i < Necklace.necklace.size(); i++) {
            System.out.println(Necklace.necklace.get(i).stoneName);
        }
    }

    /**
     * The user cleans the specified stone.
     *
     * @throws IOException We use to pass the value of a variable.
     */
    static void clear() throws IOException {
        Stone.stones.clear();
        new File("stones.dat").delete();
        out = new ObjectOutputStream(
                new FileOutputStream("stones.dat", true));
    }

    /**
     * User searches for a stone in the database.
     *
     * @param command We use to pass the value of a variable.
     * @throws InvalidCommandException We use to pass the value of a variable.
     */
    static void find(String command) throws InvalidCommandException {
        Pattern p = Pattern.compile("^.*\\s.*-.*$");
        Matcher m = p.matcher(command);
        if (!m.matches()) {
            throw new InvalidCommandException("Invalid command");
        }
        Transparency e1, e2;
        try {
            e1 = Transparency.getTransparency(command.substring(command.indexOf(' ')
                    + 1, command.indexOf('-')));
        } catch (InvalidCommandException e) {
            e.printMessage();
            return;
        }
        try {
            e2 = Transparency.getTransparency(
                    command.substring(command.indexOf('-') + 1));
        } catch (InvalidCommandException e) {
            e.printMessage();
            return;
        }

        Necklace.stonesFromRange(e1, e2);
    }

    /**
     * User delete your params.
     *
     * @param stoneName We use to pass the value of a variable.
     * @throws IOException We use to pass the value of a variable.
     */
    static void delete(String stoneName) throws IOException {
        stoneName = stoneName.substring(stoneName.indexOf(' ') + 1);
        Pattern p = Pattern.compile("^\\d+$");
        Matcher m = p.matcher(stoneName);
        if (m.matches()) {
            deleteById(Integer.parseInt(stoneName));
            return;
        }
        Iterator<Stone> iterator = Stone.stones.listIterator();
        while (iterator.hasNext()) {
            Stone temp = iterator.next();
            if (temp.stoneName.equals(stoneName)) {
                Stone.stones.remove(temp);
                int size = Necklace.necklace.size();
                Iterator<Stone> iterator1 = Necklace.necklace.listIterator();
                while (iterator1.hasNext()) {
                    if (iterator1.next().equals(temp)) {
                        iterator1.remove();
                    }
                }
            }
        }
        new File("stones.dat").delete();
        out = new ObjectOutputStream(
                new FileOutputStream("stones.dat", true));
        iterator = Stone.stones.iterator();
        while (iterator.hasNext()) {
            out.writeObject(iterator.next());
        }
    }

    /**
     * User change objects.
     *
     * @param objectToChange We use to pass the value of a variable.
     * @throws IOException We use to pass the value of a variable.
     */
    private static void changeObject(Stone objectToChange) throws IOException {
        Iterator<Stone> iterator = Stone.stones.listIterator();
        objectToChange.amount++;
        new File("stones.dat").delete();
        out = new ObjectOutputStream(
                new FileOutputStream("stones.dat", true));
        iterator = Stone.stones.iterator();
        while (iterator.hasNext()) {
            out.writeObject(iterator.next());
        }

        iterator = Necklace.necklace.iterator();
        while (iterator.hasNext()) {
            Stone temp = (Stone) iterator.next();
            if (temp.equals(objectToChange)) {
                temp.amount++;
            }
        }
    }

    /**
     * @param command We use to pass the value of a variable.
     * @throws InvalidCommandException We use to pass the value of a variable.
     */
    static void display(final String command) throws InvalidCommandException {
        Pattern p = Pattern.compile("^.*\\s.*$");
        Matcher m = p.matcher(command);
        if (!m.matches()) {
            throw new InvalidCommandException("Invalid command");
        }
        if (command.substring(command.indexOf(' ') + 1).equals("necklace")) {
            for (Stone stone : Necklace.necklace) {
                System.out.println(stone.toString());
            }
        } else if (command.substring(command.indexOf(' ') + 1).equals("stones")) {
            for (Stone stone : Stone.stones) {
                System.out.println(stone.toString());
            }
        } else {
            throw new InvalidCommandException("Invalid command");
        }
    }

    /**
     * @param command We use to pass the value of a variable.
     * @return stoneName, amount, weight, price, value, transparency.
     * @throws InvalidCommandException We use to pass the value of a variable.
     */
    private static Stone formStoneObject(String command) throws InvalidCommandException {
        final String stoneName;
        final int amount;
        final double weight;
        final double price;
        final Value value;
        final Transparency transparency;
        final int id;
        command = command.substring(command.indexOf(' ') + 1);
        stoneName = command.substring(0, command.indexOf(' '));
        command = command.substring(command.indexOf('=') + 1);
        amount = Integer.parseInt(command.substring(0, command.indexOf(' ')));
        command = command.substring(command.indexOf('=') + 1);
        weight = Double.parseDouble(command.substring(0, command.indexOf(' ')));
        command = command.substring(command.indexOf('=') + 1);
        price = Double.parseDouble(command.substring(0, command.indexOf(' ')));
        command = command.substring(command.indexOf('=') + 1);
        value = Value.getValue(command.substring(0, command.indexOf(' ')));
        command = command.substring(command.indexOf('=') + 1);
        transparency = Transparency.getTransparency(command);

        return new Stone(stoneName, amount, weight, price, value, transparency);
    }

    /**
     * @param stoneId We use to pass the value of a variable.
     * @throws IOException We use to pass the value of a variable.
     */
    private static void deleteById(int stoneId) throws IOException {
        Iterator<Stone> iterator = Necklace.necklace.listIterator();
        while (iterator.hasNext()) {
            Stone temp = iterator.next();
            if (temp.id == stoneId) {
                iterator.remove();
            }
        }
        new File("stones.dat").delete();
        out = new ObjectOutputStream(
                new FileOutputStream("stones.dat", true));
        iterator = Stone.stones.iterator();
        while (iterator.hasNext()) {
            out.writeObject(iterator.next());
        }
    }
}
