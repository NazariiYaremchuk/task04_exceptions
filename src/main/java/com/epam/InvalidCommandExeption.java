package com.epam;

/**
 * Package for my developing.
 *
 * @author Nazarii Yaremchuk.
 * @since 15-11-2019.
 */


class InvalidCommandException extends Exception {
    /**
     * Value message of type String.
     */
    private String message;

    /**
     * Indicetes incorrect command line input
     * @param s value of message
     */
    public InvalidCommandException(final String s) {
        message = s + " isn`t correct line.";
    }

    /**
     * method prints our message.
     */
    public void printMessage() {
        System.out.println(message);
    }
}
