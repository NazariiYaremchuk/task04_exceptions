package com.epam.exceptions;

public class IrrcorectStoneDataException extends RuntimeException {
    private String message;

    public IrrcorectStoneDataException(String irrcorectVariable) {
        message = "Variable " +
                irrcorectVariable + " is not revalent";
    }

    @Override
    public void printStackTrace() {
        System.out.println(message);
    }
}
