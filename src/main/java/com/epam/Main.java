package com.epam;

import java.io.IOException;
import java.util.Scanner;

/**
 * Package for my developing.
 *
 * @author Nazarii Yaremchuk.
 * @since 15-11-2019.
 */

public class Main {
    /**
     * The lab program implements implements the application.
     * that form determine the hierarchy of precious and semiprecious stones.
     * Selects stones for necklace.
     * Calculates the total weight (in carats) and cost.
     * Sortes the necklace stones by value.
     * Findes stones in necklaces that match the specified transparency setting.
     * Selects stones for necklace.
     *
     * @param args You can enter some String parameters from command line.
     * @throws IOException             can.
     * @throws ClassNotFoundException  can.
     * @throws InvalidCommandException can.
     */
    public static void main(final String[] args)
            throws IOException, ClassNotFoundException,
            InvalidCommandException {
        Stone.loadFromFile();
        Commands.viewHelp();
        System.out.println("\nEnter commands:");
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            String firstWord;
            try {
                firstWord = command.substring(0, command.indexOf(' '));
            } catch (Exception e) {
                firstWord = command;
            }
            try {
                switch (firstWord) {
                    case "help":
                        Commands.viewHelp();
                        break;
                    case "add":
                        Commands.add(command);
                        break;
                    case "create":
                        Commands.create(command);
                        break;
                    case "weight":
                        System.out.println(Necklace.totalNecklaceWeight());
                        break;
                    case "price":
                        System.out.println(Necklace.totalNecklacePrice());
                        break;
                    case "sort":
                        Necklace.sortNecklaceByValue();
                        break;
                    case "find":
                        Commands.find(command);
                        break;
                    case "delete":
                        Commands.delete(command);
                        break;
                    case "clear":
                        Commands.clear();
                        break;
                    case "exit":
                        return;
                    case "display":
                        Commands.display(command);
                        break;
                    default:
                        throw new InvalidCommandException(firstWord);
                }
            } catch (InvalidCommandException e) {
                e.printMessage();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
