package com.epam;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/**
 * Package for my developing.
 *
 * @author Nazarii Yaremchuk.
 * @since 15-11-2019.
 */
public class Necklace {
    /**
     *
     */
    public static LinkedList<Stone> necklace = new LinkedList<Stone>();

    /**
     * Constuctor to class Necklace.
     *
     * @param amountOfStones A variable that counts the number of stones.
     */
    public Necklace(int amountOfStones) {
        Random random = new Random();
        while (amountOfStones != 0) {
            int index = random.nextInt(Stone.stones.size());
            Stone temp = Stone.stones.get(index);
            Stone temp2 = new Stone(temp.stoneName, temp.amount,
                    temp.weight, temp.price, temp.value, temp.transparency);
            temp2.id = random.nextInt(99999);
            necklace.add(temp2);
            amountOfStones--;
        }
    }

    /**
     * Method counts the number of weight stone.
     *
     * @return total Weight of stone.
     */
    public static double totalNecklaceWeight() {
        double totalWeight = 0;
        for (Stone stone : necklace) {
            totalWeight += stone.weight;
        }
        return totalWeight;
    }

    /**
     * Method counts the number of price stone.
     *
     * @return total price of stone.
     */
    public static double totalNecklacePrice() {
        double totalPrice = 0;
        for (Stone stone : necklace) {
            totalPrice += stone.price;
        }
        return totalPrice;
    }

    /**
     * Method counts the number of short Necklace By Value.
     */
    public static void sortNecklaceByValue() {
        Iterator<Stone> iterator = necklace.listIterator();
        for (int i = 0; i < 7; i++) {
            for (Stone stone : necklace) {
                if (stone.value == Value.values()[i]) {
                    System.out.println(stone.stoneName);
                }
            }
        }
    }

    /**
     * method checks whether stones from the spine.
     *
     * @param e1 minimum degree of transparency.
     * @param e2 maximum degree of transparency.
     */
    public static void stonesFromRange(Transparency e1, Transparency e2) {
        for (Stone temp : necklace) {
            if ((temp.transparency.ordinal() >= e1.ordinal()) &&
                    (temp.transparency.ordinal() <= e2.ordinal())) {
                System.out.println(temp.stoneName);
            }
        }
    }
}
